package com.eniro

import slinky.core.{Component, FunctionalComponent}
import slinky.core.annotations.react
import slinky.core.facade.ReactElement
import slinky.web.html._

@react object AccountViewer {
  case class Props(accounts: Array[Account])
  val component: FunctionalComponent[Props] =
    FunctionalComponent[Props] { props =>
      table(className := "accounts")(
        tr(
          th("Id"),
          th("Name"),
          th("Email"),
          th("EcoId")
        ),
        tbody(id := "accountsBody")(
          props.accounts.map(acc =>
            tr(className := "account")(
              td(acc.id),
              td(acc.name),
              td(acc.email__c),
              td(acc.eco_id__c)
            )
          ): _*
        )
      )
    }
}
