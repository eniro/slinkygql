package com.eniro

import slinky.core._
import slinky.core.annotations.react
import slinky.web.html._

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import org.scalajs.dom
import dom.ext.Ajax
import org.scalajs.dom.ext.Ajax.InputData
import slinky.core.facade.ReactElement

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@JSImport("resources/App.css", JSImport.Default)
@js.native
object AppCSS extends js.Object

@JSImport("resources/logo.svg", JSImport.Default)
@js.native
object ReactLogo extends js.Object

@js.native
trait SearchResults extends js.Object {
  def data: SalesforceAccount
}

@js.native
trait SalesforceAccount extends js.Object {
  def salesforce_account: js.Array[Account]
}

@js.native
trait Account extends js.Object {
  def id: Int
  def country_code__c: String
  def eco_id__c: String
  def email__c: String
  def name: String
  def mitteniro_new_created__c: String
}

@react class App extends Component {
  type Props = Unit
  case class State(accounts: Array[Account])

  def initialState: State = State(Array.empty[Account])

  private val css = AppCSS

  val query: String = """{"query": "{
                  salesforce_account(where: {email__c: {_is_null: false}}) {
                    country_code__c
                    eco_id__c
                    email__c
                    id
                    name
                    mitteniro_new_created__c
                   }
              }"}""".stripMargin

  def caller(): Future[Array[Account]] = Ajax
    .post(
      "https://gql-salesforce.herokuapp.com/v1/graphql",
      query,
      0,
      Map(
        "content-type" -> "application/json",
        "x-hasura-admin-secret" -> "sw3d3nFr0mAl!c4nt3"
      )
    )
    .map { xhr => {
      val results = js.JSON.parse(xhr.responseText).asInstanceOf[SearchResults]
      val accs = results.data.salesforce_account
      println(s"Results: ${accs.length}")
      accs.toArray
    } }

  def render(): ReactElement = {
    div(className := "App")(
      header(className := "App-header")(
        img(
          src := ReactLogo.asInstanceOf[String],
          className := "App-logo",
          alt := "logo"
        ),
        h1(className := "App-title")("Salesforce Demo")
      ),
      p(className := "App-intro")("Call GQL API"),
      button(className := "myButton", onClick := (_ => {
        val fAccts = caller()
        fAccts.foreach(arr => setState(State(arr)))
      }))(
        "Click Me!"
      ),
      AccountViewer(accounts = state.accounts)
    )
  }
}
